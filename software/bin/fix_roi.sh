#!/bin/bash

SOURCE=$STUDYDIR/study-automation/support/empty.roi
ROI=$1

if [[ -s $ROI ]] ; then
  echo "$ROI is ok. Exiting"
else
  echo "$ROI is empty. Fixing."
  cp $SOURCE $ROI
fi
