#!/bin/env python

import pickle
import numpy
import sklearn
import sklearn.linear_model
import sklearn.datasets
import sklearn.cross_validation
import sklearn.ensemble
import sklearn.metrics

data = numpy.load('training_data.npy')

print "Data shape: {}".format(data.shape)

m = data.shape[0]
features = data.shape[-1] - 1 # Last is class
X = data[:,range(features)]
Y = data[:,(features)]

classifier = sklearn.linear_model.SGDClassifier(loss="modified_huber", penalty="l2", shuffle=True,
        random_state=1, n_jobs=-1, class_weight="auto", verbose="10", n_iter=5)

print "Training now..."
classifier.fit(X,Y)

pickle.dump(classifier,open('cfsgd.pic','wb'))
