#!/bin/bash

img1=$1
img2=$2
out=$3
tdir=`mktemp -d`

fsl5.0-slicer $1 -a ${tdir}/1.pgm
fsl5.0-slicer $2 -a ${tdir}/2.pgm

convert ${tdir}/1.pgm ${tdir}/1.gif
convert ${tdir}/2.pgm ${tdir}/2.gif
fsl5.0-whirlgif -o ${out} -loop -time 100 ${tdir}/1.gif ${tdir}/2.gif
