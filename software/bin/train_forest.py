#!/bin/env python

import pickle
import numpy
import sklearn
import sklearn.datasets
import sklearn.cross_validation
import sklearn.ensemble
import sklearn.metrics

data = numpy.load('training_data.npy')

print "Data shape: {}".format(data.shape)

m = data.shape[0]
features = data.shape[-1] - 1 # Last is class
X = data[:,range(features)]
Y = data[:,(features)]

classifier = sklearn.ensemble.RandomForestClassifier(n_estimators=20, max_depth=20,
	max_features=0.5, random_state=1, verbose=10, n_jobs=-1)


rand = numpy.random.permutation(m)

train_frac = 0.8

split_point = int(m * 0.8)

Xrt = X[rand[0:split_point]]
Yrt = Y[rand[0:split_point]]

Xrv = X[rand[split_point:-1]]
Yrv = Y[rand[split_point:-1]]

print "Training now..."
classifier.fit(X,Y)

#terrs = numpy.zeros(50)
#cerrs = numpy.zeros(50)
#for i in range(1,500000,10000):
#  classifier.fit(Xrt[0:i],Yrt[0:i])
#  terrs[i/10000] = 1 - sklearn.metrics.f1_score(Yrt[0:i], classifier.predict(Xrt[0:i]))
#  cerrs[i/10000] = 1 - sklearn.metrics.f1_score(Yrv, classifier.predict(Xrv))
#print terrs
#print cerrs

pickle.dump(classifier,open('cf.pic','wb'))
