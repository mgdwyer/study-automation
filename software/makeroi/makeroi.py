#!/bin/env python

import optparse
import datetime
import numpy
import math
import nibabel
import matplotlib

#matplotlib.use('PS')

import matplotlib.pyplot as plt

usage = "usage: %prog [options] image mask"
parser = optparse.OptionParser(usage=usage)
parser.add_option("-v", "--verbose", dest="verbose",
        action="store_true", default=False, help="Increase verbosity")

(options, args) = parser.parse_args()

if len(args) != 2:
    print "You must supply a mask, and an output"
    exit()

timestamp = datetime.datetime.today().strftime("%d %b %Y %H:%m:%S")

mask = nibabel.load(args[0])

header = mask.get_header()
zooms = header.get_zooms()

mask_data = mask.get_data()

x = mask_data.shape[0]
y = mask_data.shape[1]
z = mask_data.shape[2]

x_off = (x * zooms[0]) / 2
y_off = (y * zooms[1]) / 2


roi_file = open(args[1], 'w')

any_rois = False

for i in range(z):
    slice = mask_data[:,:,i]
    if options.verbose:
        print "Processing slice {0}".format(i)
    contours = plt.contour(numpy.fliplr(slice), [0.5], origin="lower").allsegs[0]
    if options.verbose:
        print "\tFound {0} ROIs".format(len(contours))
    for contour in contours:
	any_rois = True
	roi_file.write('Begin Irregular ROI\n')
  	roi_file.write('  Build version=\"3.0_x\"\n')
	roi_file.write('  Annotation=""\n')
        roi_file.write('  Image source="none.nii.gz"\n')
        roi_file.write('  Slice={0}\n'.format(i + 1))
        roi_file.write('  Created "{0}" by Operator ID="AUTOCONV"\n'.format(timestamp))
        roi_file.write('  Begin Shape\n')
        roi_file.write('    Points={0}\n'.format(contour.shape[0]))
        for point in contour:
            roi_file.write('    X={0}; Y={1}\n'.format(point[1]*zooms[1] - y_off, point[0]*zooms[0] - x_off)) #TODO:Resulting in ROIs that have a boundary 1 voxel past the lesion mask...
        roi_file.write('  End Shape\n')
        roi_file.write('End Irregular ROI\n')


# If there are no contours, make a single-point ROI on the bottom slice.
# This is just to deal with the annoying aspect of JIM that it won't 
# actually load an ROI file with no ROIs in it.

if any_rois != True:
    roi_file.write('Begin Marker ROI\n')
    roi_file.write('  Build version="5.0_23"\n')
    roi_file.write('  Annotation=""\n')
    roi_file.write('  Colour=0\n')
    roi_file.write('  Image source=""\n')
    roi_file.write('  Slice=1\n')
    roi_file.write('  Created  "{0}" by Operator ID="AUTOCONVZERO"\n'.format(timestamp))
    roi_file.write('  Begin Shape\n')
    roi_file.write('    X=0; Y=0\n')
    roi_file.write('  End Shape\n')
    roi_file.write('End Marker ROI\n')


roi_file.close

