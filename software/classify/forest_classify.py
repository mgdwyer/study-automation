#!/bin/env python

import optparse
import numpy
import nibabel
import pickle

import sys
import os

sys.path.insert(0, os.environ["STUDYDIR"] + "/software")

import treelearn

usage = "usage: %prog [options] feature_image forest_def output"
parser = optparse.OptionParser(usage=usage)
parser.add_option("-v", "--verbose", dest="verbose",
	action="store_true", default=False, help="Increase verbosity")

(options, args) = parser.parse_args()

if len(args) != 3:
    print "You must supply a 4D feature image, a forest definition, and an output file"
    exit()

output = args[2]

print "Loading forest..."
stream = file(args[1], 'r')
forest = pickle.load(stream)

print "Loading feature image..."
simg = nibabel.load(args[0])
features = simg.get_data()

x = features.shape[0]
y = features.shape[1]
z = features.shape[2]

n = features.shape[-1]
print "Found {0} features.".format(n)
print "Rehaping vectors...",
fvecs = []
for i in range(n):
    fvecs.append(features[:,:,:,i].reshape(-1))
print "done."

farray = numpy.column_stack(fvecs)

print "Array size is: {0}".format(farray.shape)

print "Classifying..."
predictions = forest.predict_proba(farray)

cdata = numpy.reshape(predictions[:,1], (x,y,z))
cimg = nibabel.Nifti1Image(cdata, simg.get_affine())
nibabel.save(cimg, output)



