#!/bin/env python

import optparse
import numpy
import sys

numpy.random.seed(1)

usage = "usage: %prog [options] output file_1 file_2 ... file_n"
parser = optparse.OptionParser(usage=usage)
parser.add_option("-v", "--verbose", dest="verbose",
	action="store_true", default=False, help="Increase verbosity")
parser.add_option("-m", "--match", dest="match", 
	action="store_true", default=False, 
	help="Match number of non-lesions to number of lesions")

(options, args) = parser.parse_args()

if len(args) < 2:
    print "You must supply an output name and at least one feature vector."
    exit()

output = args[0]

print "Compiling data..."

data = numpy.load(args[1])

for fn in args[2:]:
  sys.stdout.write('.')
  sys.stdout.flush()
  data = numpy.vstack([data, numpy.load(fn)])
print ""

print "Raw dims: {}".format(data.shape)

islesion = data[:,(-1)] > 0
lesions = data[islesion]
nonlesions = data[numpy.invert(islesion)]

print "Lesions: {}".format(lesions.shape)
print "Non-lesions: {}".format(nonlesions.shape)

if options.match:
  num_lesions = lesions.shape[0]
  num_nonlesions = nonlesions.shape[0]
  idxes = numpy.random.permutation(num_nonlesions)
  nonlesions = nonlesions[idxes[0:(num_lesions*3)]]
  data = numpy.vstack([lesions,nonlesions])

print "Final dims: {}".format(data.shape)


print "Writing..."
numpy.save(output, data)

