#!/bin/env python

import nibabel
import optparse
import numpy

usage = "usage: %prog [options] 4d-feature-image label-image output-name"
parser = optparse.OptionParser(usage=usage)
parser.add_option("-v", "--verbose", dest="verbose",
	action="store_true", default=False, help="Increase verbosity")

(options, args) = parser.parse_args()

if len(args) != 3:
    print "You must supply a feature image, a label image, and an output name."
    exit()

output = args[2]

print "Loading images...",
features = nibabel.load(args[0]).get_data()
labels = nibabel.load(args[1]).get_data()
print "done."

n = features.shape[-1]
print "Found {} features.".format(n)
print "Rehaping vectors...",
fvecs = []
for i in range(n):
    fvecs.append(features[:,:,:,i].reshape(-1))
print "done."
fvecs.append(labels.reshape(-1))

farray = numpy.column_stack(fvecs)

print "Raw array size is:"
print farray.shape

print "Trimming masked-out rows..."
good = farray[:,0] > 0
farray_m = farray[good]

print "Final array size is:"
print farray_m.shape

numpy.save(output, farray_m)


