#!/bin/env python

import sys
import nibabel
import optparse
import numpy
import scipy
from scipy import ndimage

usage = "usage: %prog [options] base-image output-prefix"
parser = optparse.OptionParser(usage=usage)
parser.add_option("-v", "--verbose", dest="verbose",
    action="store_true", default=False, help="Increase verbosity")

(options, args) = parser.parse_args()

if len(args) != 2:
  print "You must supply a base image and an output prefix."
  exit()

output_prefix = args[1]

print "Loading image...",

nifti = nibabel.load(args[0])
base_image = numpy.array(nifti.get_data(), dtype='float32')
affine = nifti.get_affine()
zooms = nifti.get_header().get_zooms()

imshape = base_image.shape

print "Creating smoothings..."

smoothings = (
    ("1", (1,1,1)),
    ("2", (2,2,2)),
    ("4", (4,4,4)),
    #("8", (8,8,8)) 
    )

# Do basic smoothings
for item in smoothings :
  label = item[0]
  kernel = item[1]
  scale_kernel = numpy.true_divide(kernel, zooms)
  print "Smoothing at ", kernel, " ", scale_kernel
  fdata = scipy.ndimage.filters.gaussian_filter(base_image, scale_kernel)
  outname = output_prefix + "_gauss" + label + ".nii.gz"
  new_image = nibabel.Nifti1Image(fdata, affine)
  nibabel.save(new_image, outname)

# Calculate gradients
for item in smoothings :
  label = item[0]
  kernel = item[1]
  scale_kernel = numpy.true_divide(kernel, zooms)
  print "Detecting gradient at ", kernel, " ", scale_kernel
  fdata = scipy.ndimage.filters.gaussian_gradient_magnitude(base_image, scale_kernel)
  outname = output_prefix + "_grad" + label + ".nii.gz"
  new_image = nibabel.Nifti1Image(fdata, affine)
  nibabel.save(new_image, outname)

# Calculate laplacians
for item in smoothings :
  label = item[0]
  kernel = item[1]
  scale_kernel = numpy.true_divide(kernel, zooms)
  print "Calculating Laplacian at ", kernel, " ", scale_kernel
  fdata = scipy.ndimage.filters.gaussian_laplace(base_image, scale_kernel)
  outname = output_prefix + "_lap" + label + ".nii.gz"
  new_image = nibabel.Nifti1Image(fdata, affine)
  nibabel.save(new_image, outname)


# Calculate Hessian
for item in smoothings :
  label = item[0]
  kernel = item[1]
  scale_kernel = numpy.true_divide(kernel, zooms)
  print "Calculating Hessian stats at ", kernel, " ", scale_kernel
  smoothed = scipy.ndimage.filters.gaussian_filter(base_image, scale_kernel)
  dxx = ndimage.filters.gaussian_filter1d(smoothed, 0.5, axis=0, order=2)
  dyy = ndimage.filters.gaussian_filter1d(smoothed, 0.5, axis=1, order=2)
  dzz = ndimage.filters.gaussian_filter1d(smoothed, 0.5, axis=2, order=2)
  dxy = ndimage.filters.gaussian_filter1d(ndimage.filters.gaussian_filter1d(smoothed, 
      0.5, axis=1, order=1), 0.5, axis=0, order=1)
  dxz = ndimage.filters.gaussian_filter1d(ndimage.filters.gaussian_filter1d(smoothed, 
      0.5, axis=2, order=1), 0.5, axis=0, order=1)
  dyz = ndimage.filters.gaussian_filter1d(ndimage.filters.gaussian_filter1d(smoothed, 
      0.5, axis=2, order=1), 0.5, axis=1, order=1)

  hessian = numpy.array( ( (dxx, dxy, dxz), (dxy, dyy, dyz), (dxz, dyz, dzz) ) )

  eig_min = numpy.zeros(imshape)
  eig_mid = numpy.zeros(imshape)
  eig_max = numpy.zeros(imshape)

  for z in range(imshape[2]):
      sys.stdout.write('.')
      sys.stdout.flush()
      for y in range(imshape[1]):
          for x in range(imshape[1]):
	      if base_image[x,y,z] > 0:
                vals = numpy.linalg.eigvalsh(hessian[:,:,x,y,z])
                vals.sort()
                eig_min[x,y,z] = vals[0]
                eig_mid[x,y,z] = vals[1]
                eig_max[x,y,z] = vals[2]
              else:
                eig_min[x,y,z] = 0
                eig_mid[x,y,z] = 0
                eig_max[x,y,z] = 0
  print('')                

  det = numpy.multiply(eig_mid, numpy.multiply(eig_min, eig_max))
  outname = output_prefix + "_hess_det" + label + ".nii.gz"
  new_image = nibabel.Nifti1Image(det, affine)
  nibabel.save(new_image, outname)

  tubeness = numpy.multiply(eig_min, eig_mid)
  tubeness[eig_min > 0] = 0
  tubeness[eig_mid > 0] = 0
  outname = output_prefix + "_hess_tube" + label + ".nii.gz"
  tubeness = numpy.sqrt(tubeness)
  new_image = nibabel.Nifti1Image(tubeness, affine)
  nibabel.save(new_image, outname)



