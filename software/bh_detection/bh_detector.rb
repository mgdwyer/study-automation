#!/bin/env ruby

require 'rubygems'
require 'trollop'
require 'image_formats'
require 'bnac_utils'


opts = Trollop::options do
  opt :t1_se, "T1 SE image (Nifti)", :type => String
  opt :fll_mask, "Binary FLL mask image (Nifti)", :short => 'l', :type => String
  opt :gd_mask, "Binary Gd lesion mask (Nifti)", :short => 'g', :type => String 
  opt :out_name, "Output mask filename", :type => String
  opt :amount, "Intensity difference for erosion (500)", :default => 500, :type => :int
end

Trollop::die :t1_se, "must be specified" unless opts[:t1_se]
Trollop::die :fll_mask, "must be specified" unless opts[:fll_mask]
Trollop::die :gd_mask, "must be specified" unless opts[:gd_mask]
Trollop::die :out_name, "must be specified" unless opts[:out_name]


t1_se = NiftiFile.grab(File.expand_path(opts[:t1_se]))
fll_mask = NiftiFile.grab(File.expand_path(opts[:fll_mask]))
gd_mask = NiftiFile.grab(File.expand_path(opts[:gd_mask]))
out_name = File.expand_path(opts[:out_name])
amount = opts[:amount]

puts "Detecting black holes..."

STDOUT.sync = true

Bnac::TempdirUtils.in_temporary_directory do 
  print "\tMaking T1-prime... "
  `fslmaths #{t1_se} -add #{amount} mask_img`
  puts "done."
  mask_img = NiftiFile.grab("mask_img")

  print "\tPerforming geodesic erosion... "
  `geodesic_erosion #{mask_img} #{t1_se} geo.nii`
  puts "done."
  geo = NiftiFile.grab("geo")

  print "\tCreating regional minima map... "
  `fslmaths #{geo} -sub #{t1_se} -bin localmin`
  puts "done."
  localmin = NiftiFile.grab("localmin")

  print "\tMasking with FLAIR lesions... "
  `fslmaths #{fll_mask} -bin -mul #{localmin} fll_masked`
  puts "done."

  print "\tCreating Gd-negative mask..."
  `fslmaths #{gd_mask} -kernel 2D -dilF -dilF -bin -mul -1 -add 1 gd_neg_mask`
  puts "done."

  print "\tApplying Gd-negative mask..."
  `fslmaths fll_masked -mas gd_neg_mask all_masked`
  puts "done."

  print "\tDoing morphological cleaning..."
  `fslmaths all_masked -kernel 2D -ero -dilF #{out_name}`
  puts "done."

end


