#!/bin/env python

import optparse
import numpy
import nibabel
from sklearn.neighbors import KNeighborsRegressor
from scipy import ndimage

# Set up the option parser to handle the command-line
usage = """%prog [options] source-img target-img output-image"

This program takes two images, source and target. It determines a non-linear
mapping from intensities in source to target, and then remaps source 
accordingly. It uses a nearest neighbors regressor to do so. Theoretically,
the "successfulness" of this operation depends on the joint histogram entropy.

Warning: this program is BNAC VRMQC, and should be used with appropriate
caution. It is not designed for use without manual review of the 
outputs."""
parser = optparse.OptionParser(usage=usage)

parser.add_option("-v", "--verbose", dest="verbose",
        help="Increase verbosity", action="store_true", default=False)

parser.add_option("-d", "--downsample-factor", dest="ds_factor",
        type="float", help="Spatial downsampling factor (0.25)", 
        metavar="FACTOR",
        default = 0.25)

(options, args) = parser.parse_args()
if len(args) != 3:
    print "You must supply source, target, and output image names"
    exit()

if options.verbose:
    def vprint(*args):
        # Print each argument separately so caller doesn't need to
        # stuff everything to be printed into a single string
        for arg in args:
           print arg,
        print
else:   
    vprint = lambda *a: None      # do-nothing function

# Load up the images with Nibabel
vprint("Loading source images")
img1 = nibabel.load(args[0])
img2 = nibabel.load(args[1])

# Get the actual data
data1 = img1.get_data()
data2 = img2.get_data()

# K-neighbors algorithms aren't the fastest, so we downsample. It's 3D, so
# the factor will be df_factor^3 -- a substantial speedup.
vprint("Downsampling data by a factor of ", options.ds_factor)
data1_small = ndimage.zoom(data1,options.ds_factor)
data2_small = ndimage.zoom(data2,options.ds_factor)

# Assume the images are already deskulled, so don't include 0's.
vprint("Removing voxels with 0 intensity")
data1_cut = data1_small[data1_small != 0]
data2_cut = data2_small[data1_small != 0]
data1_cut_f = data1_cut.flatten()
data2_cut_f = data2_cut.flatten()

# Set up a scikit-based K-nearest neighbors regressor
neigh = KNeighborsRegressor()
vprint("Fitting model")
neigh.fit(numpy.transpose(numpy.atleast_2d(data1_cut_f)),data2_cut_f)


# To speed things up, since there are many less intensities than voxels, we
# first create a lookup table.
vprint("Creating lookup table")
full_data1 = numpy.transpose(numpy.atleast_2d(data1.flatten()))
vals = numpy.sort(numpy.unique(full_data1))
dvals = numpy.transpose(numpy.atleast_2d(vals))
predvals = neigh.predict(dvals)
vdict = dict(numpy.vstack((vals, predvals)).T)
vdict[0] = 0

# Then apply the table to the raw source image data
vprint("Applying transform")
data1_trans = [(vdict[i]) for i in data1.flatten()]
data1_trans_3d = numpy.reshape(data1_trans, data1.shape)

# Save the output file
vprint("Saving output")
oimg = nibabel.Nifti1Image(data1_trans_3d, img1.get_affine())
nibabel.save(oimg, args[2])




