#!/bin/env ruby

require 'rubygems'
require 'trollop'
require 'image_formats'
require 'bnac_utils'


opts = Trollop::options do
  opt :t1_se, "T1 SE image (Nifti)", :type => String, :short => 't'
  opt :t1_se_gd, "T1 SE image (Nifti)", :type => String, :short => 'g'
  opt :fll_mask, "Binary FLL mask image (Nifti)", :short => 'l', :type => String
  opt :out_name, "Output mask filename", :type => String
  opt :amount, "Intensity difference for erosion (500)", :default => 500, :type => :int
end

Trollop::die :t1_se, "must be specified" unless opts[:t1_se]
Trollop::die :t1_se_gd, "must be specified" unless opts[:t1_se_gd]
Trollop::die :fll_mask, "must be specified" unless opts[:fll_mask]
Trollop::die :out_name, "must be specified" unless opts[:out_name]


t1_se = NiftiFile.grab(File.expand_path(opts[:t1_se]))
t1_se_gd = NiftiFile.grab(File.expand_path(opts[:t1_se_gd]))
fll_mask = NiftiFile.grab(File.expand_path(opts[:fll_mask]))
out_name = File.expand_path(opts[:out_name])
amount = opts[:amount]

puts "Detecting Gd lesions..."

STDOUT.sync = true

Bnac::TempdirUtils.in_temporary_directory do 
  print "\tMaking subtraction... "
  `fslmaths #{t1_se_gd} -sub #{t1_se} -mul -1 -add 2000 sub_img`
  puts "done."
  sub_img = NiftiFile.grab("sub_img")

  print "\tMaking mask image..."
  `fslmaths #{sub_img} -add #{amount} mask_img`
  puts "done."
  mask_img = NiftiFile.grab("mask_img")

  print "\tPerforming geodesic erosion... "
  `geodesic_erosion #{mask_img} #{sub_img} geo.nii`
  puts "done."
  geo = NiftiFile.grab("geo")

  print "\tCreating regional minima map... "
  `fslmaths #{geo} -sub #{sub_img} -bin localmin`
  puts "done."
  localmin = NiftiFile.grab("localmin")

  print "\tMasking with T2 lesions and doing morphological cleaning... "
  `fslmaths #{fll_mask} -bin -mul #{localmin} -kernel 2D -ero -dilF #{out_name}` 
  puts "done."
end


