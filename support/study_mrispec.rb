NEEDED = [
  ["flair", 
    p = Proc.new{ |x|
      m1 = (/flair/i).match(x) 
      m2 = (/SC:/i).match(x)
      m3 = (/3D/i).match(x)
      m4 = (/AX/i).match(x)
      m1 and not m2 and not m3 and m4
    }
  ],

  ["t1", 
    p = Proc.new{ |x|
      m1 = (/AX/i).match(x) 
      m2 = (/T1/i).match(x) 
      m3 = (/SC:/i).match(x)
      m1 and m2 and not m3
    }
  ],

  ["t1gd", 
    p = Proc.new{ |x|
      m1 = (/AX/i).match(x) 
      m2 = (/g[a]?d/i).match(x)
      m3 = (/SC:/i).match(x)
      m1 and m2 and not m3
    }
  ],

  ["t2", 
    p = Proc.new{ |x|
      m1 = (/Echo 2/i).match(x) 
      m2 = (/SC:/i).match(x)
      m1 and not m2
    }
  ],

  ["pd", 
    p = Proc.new{ |x|
      m1 = (/Echo 1/i).match(x) 
      m2 = (/SC:/i).match(x)
      m1 and not m2
    }
  ],

  ["spgr", 
    p = Proc.new{ |x|
      m1 = (/SPGR/i).match(x) 
      m2 = (/SC:/i).match(x)
      m1 and not m2
    }
  ],
]
